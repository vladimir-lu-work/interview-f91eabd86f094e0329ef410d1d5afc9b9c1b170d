
## Notes on the python implementation

In order to be more pythonic, the only deviation that was made is the removal of the initialisation of instance variables
from the class body and into the `__init__` method, as well as additional validation on the inputs. Additionally, the
number and the probability it occurs at were wrapped in a `NamedTuple` instead of being specified separately.

If the `RandomGen` class was to continue having a single method, then it would probably be better off being a function
object or even a generator, depending on the expected usage pattern.

A number of libraries could have helped shorten the implementation but it was assumed libraries outside the standard
library could not be used. Python 2.7 was chosen for maximum compatibility.

### Testing

It would have been nice to have SciPy or similar library available to test the random distributions. The current tests
are expected to fail once in a while because of the primitive method of coming up with the p-value. In addition, the
tests should probably run multiple times, and with the random generator seeded at the start as well.

(And sure enough, running the tests on a different day causes random test failures).