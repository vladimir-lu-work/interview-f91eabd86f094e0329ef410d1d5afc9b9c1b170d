import random

from bisect import bisect
from collections import namedtuple


def cumulative_sum(number_list):
    """
    Compute a cumulative sum given a list of numbers
    :param number_list: The list of numbers
    :return: The list containing the cumulative sum
    """

    # (python 3.2+ for itertools.accumulate)
    def accumulate(pair, next):
        (result_list, sum) = pair
        sum += next
        result_list.append(sum)
        return result_list, sum

    (results, _) = reduce(accumulate, number_list, ([], 0.0))
    return results


class NumberProbabilityPair(namedtuple('_NumberProbabilityPair', ['num', 'prob'])):
    """
    A tuple of (number, expected probability)
    """
    pass


class RandomGen(object):
    def __init__(self, num_pairs):
        """
        Create a new random generator that will sample random numbers from a list with the given probability (with
            replacement)
        :param num_pairs: A sequence of `NumberProbabilityPair`
        :type num_pairs: list of NumberProbabilityPair
        """
        self._random_nums = [p.num for p in num_pairs]
        self._probabilities = [p.prob for p in num_pairs]

        # (google hint: apparently python 3.5 now has math.isclose)
        sum_probs = sum(self._probabilities, 0.0)
        if abs(sum_probs - 1.0) > 0.001:
            raise ValueError("The probabilities do not all add up to 1, instead got: {}".format(sum_probs))

        self._cumulative_probabilities = cumulative_sum(self._probabilities)

    def next_num(self):
        """
        Picks one of the random numbers given to the generator, according to the probability assigned to it
        """
        # because of the assumption the random function is evenly distributed, we can take a
        prob = random.random()
        idx = bisect(self._cumulative_probabilities, prob)
        if idx >= len(self._random_nums):
            raise IndexError(
                "Probability index {} out of range ,{}) - this is probably a bug".format(idx, len(self._random_nums)))

        return self._random_nums[idx]
