from interview.random_num import cumulative_sum, RandomGen, NumberProbabilityPair

from collections import defaultdict
from itertools import groupby, repeat
from math import sqrt


def expected_distribution(probability_pairs, num_samples):
    """
    Compute the expected distribution given a set of numbers with their probabilities and the size of the sample
    """
    # TODO: do not take as many liberties with rounding
    return {p.num: int(round(p.prob * num_samples)) for p in probability_pairs}


def chi_squared_statistic(expected_dist, got_dist):
    """
    Compute the chi-squared statistic for a given expected distribution
    """
    results = []
    for (num, expected_count) in expected_dist.iteritems():
        got_count = got_dist[num]
        results.append((got_count - expected_count) ** 2 / (1.0 * expected_count))
    return sum(results)


def is_chi_squared_probable(statistic, dof):
    """
    Check whether the computed chi-squared statistic is probable

    Lacking access to scipy or similar, we don't test against the chi-squared distribution but instead against a simple
    test as found in Sedgewick
    :param statistic: The statistic, as computed by `chi_squared_statistic`
    :param dof: Number of degrees of freedom
    :return: True if it is, False otherwise
    """
    # FIXME: use a better method (i.e. chi-squared distribution not this)
    return abs(statistic - dof) <= 2 * sqrt(dof)


def test_chi_squared():
    """
    Very simplistic test for our chi-squared function above (taking an example from wikipedia)
    """
    val = chi_squared_statistic({
        'm': 500,
        'f': 500,
    }, {
        'm': 524,
        'f': 486,
    })
    assert is_chi_squared_probable(val, 1) is True


def test_cumulative_sum():
    """
    Test the cumulative sum function adds numbers up correctly
    """
    assert cumulative_sum([]) == []
    assert cumulative_sum([0.0]) == [0.0]
    assert cumulative_sum([0.0, 1.0]) == [0.0, 1.0]
    assert cumulative_sum([1.0, 2.0]) == [1.0, 3.0]
    assert cumulative_sum(range(0, 10)) == [0.0, 1.0, 3.0, 6.0, 10.0, 15.0, 21.0, 28.0, 36.0, 45.0]
    # ugly hack for succinct test
    assert map(str, cumulative_sum(repeat(0.1, 10))) == map(str, [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])


def test_simple_generation():
    """
    Test the random generator works for an even distribution of ones and zeros
    """
    probability_pairs = [
        NumberProbabilityPair(0, 0.3),
        NumberProbabilityPair(1, 0.7)
    ]
    samples = 1000
    expected = expected_distribution(probability_pairs, samples)

    gen = RandomGen(probability_pairs)
    nums = map(lambda _: gen.next_num(), range(0, samples))
    got = defaultdict(lambda: 0, {k: len(list(v)) for (k, v) in groupby(sorted(nums))})

    assert is_chi_squared_probable(chi_squared_statistic(expected, got), 1) is True


def test_unfair_dice():
    """
    Test that the random number generator can produce an unfair dice
    """
    produced_probability_pairs = [
        NumberProbabilityPair(1, 0.1),
        NumberProbabilityPair(2, 0.1),
        NumberProbabilityPair(3, 0.1),
        NumberProbabilityPair(4, 0.2),
        NumberProbabilityPair(5, 0.2),
        NumberProbabilityPair(5, 0.3),
    ]

    fair_prob = 1.0 / 6
    expected_probability_pairs = [
        NumberProbabilityPair(1, fair_prob),
        NumberProbabilityPair(2, fair_prob),
        NumberProbabilityPair(3, fair_prob),
        NumberProbabilityPair(4, fair_prob),
        NumberProbabilityPair(5, fair_prob),
        NumberProbabilityPair(5, fair_prob),
    ]

    samples = 1000
    expected = expected_distribution(expected_probability_pairs, samples)

    gen = RandomGen(produced_probability_pairs)

    nums = map(lambda _: gen.next_num(), range(0, samples))
    got = defaultdict(lambda: 0, {k: len(list(v)) for (k, v) in groupby(sorted(nums))})

    # this is slightly unfair because we're not testing any threshold but nevertheless this should always pass
    assert is_chi_squared_probable(chi_squared_statistic(expected, got), 5) is False


def test_given_problem():
    """
    Test that the problem given in the interview sheet can be roughly reproduced
    """

    probability_pairs = [
        NumberProbabilityPair(-1, 0.01),
        NumberProbabilityPair(0, 0.3),
        NumberProbabilityPair(1, 0.58),
        NumberProbabilityPair(2, 0.1),
        NumberProbabilityPair(3, 0.01),
    ]

    samples = 100
    expected = expected_distribution(probability_pairs, samples)

    gen = RandomGen(probability_pairs)

    # TODO - this has been duplicated across all the tests above, refactor
    nums = map(lambda _: gen.next_num(), range(0, samples))
    got = defaultdict(lambda: 0, {k: len(list(v)) for (k, v) in groupby(sorted(nums))})

    assert is_chi_squared_probable(chi_squared_statistic(expected, got), 4) is True
