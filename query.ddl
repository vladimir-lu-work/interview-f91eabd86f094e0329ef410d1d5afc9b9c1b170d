-- sqlite syntax
-- insert structure + data
CREATE TABLE product (
  product_id     INT PRIMARY KEY,
  name           VARCHAR(128) NOT NULL,
  rrp            DECIMAL      NOT NULL,
  available_from DATE         NOT NULL
);

CREATE TABLE orders (
  order_id      INT PRIMARY KEY,
  product_id    INT     NOT NULL,
  quantity      INT     NOT NULL,
  order_price   DECIMAL NOT NULL,
  dispatch_date DATE    NOT NULL,
  FOREIGN KEY (product_id) REFERENCES product (product_id)
);

INSERT INTO product (product_id, name, rrp, available_from) VALUES
  (101, 'Bayesian methods for nonlinear...', 94.95, date('now', '-7 days', 'weekday 4')),
  (102, '(next year) in Review (preorder)', 21.95, date('now', '+1 year')),
  (103, 'Learn Python (badly) in Ten Minutes', 2.15, date('now', '-3 months')),
  (104, 'sports almanac', 3.38, date('now', '-3 years')),
  (105, 'finance for dummies and pension funds', 84.99, date('now', '-1 year'));


INSERT INTO orders (order_id, product_id, quantity, order_price, dispatch_date) VALUES
  (1000, 101, 1, 90.0, date('now', '-2 months')),
  (1001, 103, 1, 1.15, date('now', '-40 days')),
  (1002, 101, 9, 90.0, date('now', '-11 months')),
  (1003, 104, 11, 3.38, date('now', '-6 months')),
  (1004, 105, 7, 501.33, date('now', '-2 years'));

-- query
SELECT p.product_id
FROM orders o
  JOIN product p ON p.product_id = o.product_id
WHERE p.available_from < date('now', '-1 month')
      AND o.dispatch_date > date('now', '-1 year')
GROUP BY p.product_id
HAVING sum(o.quantity) < 10;
